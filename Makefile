default:
	@echo "No Default make command configured"
	@echo "Please use either"
	@echo "   - make curseforge"
	@echo "   - make multimc"
	@echo "   - make technic"
	@echo "   - make all"
	@echo ""
	@echo "Curseforge will make a curseforge compatible zip"
	@echo "Multimc will make a multimc zip file which contains"
	@echo "   the packwiz updater"
	@echo ""
	@echo "Technic will make a technic pack zip"
	@echo ""
	@echo "All will make all packs it can"
	@echo ""
	
curseforge:
	@echo "Making Curseforge pack"
	packwiz curseforge export --pack-file .minecraft/pack.toml -o ../wof-curseforge.zip
	7z d ../wof-curseforge.zip overrides/packwiz-installer-bootstrap.jar overrides/pack.toml  overrides/index.toml	

modrinth:
	@echo "Making Modrinth pack"
	packwiz modrinth export --pack-file .minecraft/pack.toml -o ../wof-modrinth.zip
	7z d ../wof-modrinth.zip overrides/packwiz-installer-bootstrap.jar overrides/pack.toml  overrides/index.toml	

multimc:
	@echo "Making MultiMC pack"
	7z d ../wof-multimc.zip ./* -r
	7z d ../wof-multimc.zip ./.minecraft -r
	7z a ../wof-multimc.zip ./* -r
	7z a ../wof-multimc.zip ./.minecraft -r
	7z d ../wof-multimc.zip ./.minecraft/mods ./.minecraft/pack.toml ./.minecraft/index.toml -r

technic:
	@echo "Making Technic pack"
	-rm -rf .technic
	-cp -r .minecraft .technic
	mv .technic/modpack.icon.png .technic/icon.png
	cd .technic && java -jar packwiz-installer-bootstrap.jar https://gitlab.com/Merith-TK/modpack-city-of-progress/-/raw/main/.minecraft/pack.toml && cd ..
	wget https://maven.minecraftforge.net/net/minecraftforge/forge/1.16.5-36.2.29/forge-1.16.5-36.2.29-installer.jar -O .technic/bin/modpack.jar
	-rm -rf .technic/packwiz*
	7z d ../wof-technic.zip ./* -r
	7z a ../wof-technic.zip ./.technic/* -r

clean:
	-rm -rf .technic
	-git gc --aggressive --prune

all: curseforge modrinth multimc technic clean
